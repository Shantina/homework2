import React, { Component } from 'react';
import moment from 'moment';
import axios from 'axios';

class Clock extends Component {
    constructor() {
        super();
        this.state = {
            time: moment().format("HH:mm:ss")
        }
    }
    componentDidMount() {
        setInterval(() => {
            this.setState({
                time: moment().format("HH:mm:ss")
            });
        }, 1000);
        axios('http://jsonplaceholder.typicode.com/users/1')
            .then((res) => console.log(res.data))
    }
    render() {
        return (
            <h1>
                {this.state.time}

            </h1>
        )
    }
}


export default Clock;
