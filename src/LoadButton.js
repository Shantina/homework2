import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Button from 'react-bootstrap/Button';


const LoadButton = ({ count, increaseCount }) => {
    return (
        <div>
            <Button onClick={() => increaseCount(count + 1)} >Load</Button>
        </div>
    )
};




export default LoadButton;