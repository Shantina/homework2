import React, { Component } from 'react';
import moment from 'moment';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
// import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Card from 'react-bootstrap/Card';
import LoadButton from './LoadButton';
import Button from 'react-bootstrap/Button';


class AxiosNew extends Component {
    constructor(props) {
        super(props);
        this.state = {
            blogpost: [],
            blogphotos: [],
            id: 1,
        }
        this.loadMore = this.loadMore.bind(this);
    }
    loadMore() {
        axios(`http://jsonplaceholder.typicode.com/posts/${this.state.id}`)
            .then((res) => {
                this.setState({
                    blogpost: [...this.state.blogpost, res.data],
                    id: this.state.id + 1
                })
            })
        axios(`http://jsonplaceholder.typicode.com/photo/${this.state.id}`)
            .then((photos) => {
                this.setState({
                    blogphotos: [...this.state.blogphotos, photos.data],
                    id: this.state.id + 1
                })
            })
    }
    componentDidMount() {
        axios(`http://jsonplaceholder.typicode.com/posts/${this.state.id}`)
            .then((res) => {
                this.setState({
                    blogpost: [...this.state.blogpost, res.data],
                    id: this.state.id + 1
                })
            })
        axios(`http://jsonplaceholder.typicode.com/photo/${this.state.id}`)
            .then((photos) => {
                this.setState({
                    blogphotos: [...this.state.blogphotos, photos.data],
                    id: this.state.id + 1
                })
            })
    }
    render() {
        console.log(this.state.blogpost);

        return (
            <div>
                <Container>
                    <Row>
                        <Button onClick={this.loadMore}>Load more</Button>
                    </Row>
                    <Row>


                        {
                            this.state.blogpost.map((item, index) => {
                                return (
                                    <Col md={4}>
                                        <div>
                                            <img className="style-img" src={this.state.blogphotos[index] ? this.state.blogphotos[index].url : ''} />
                                            <h5>{item.title}</h5>
                                            <p> {item.body}</p>
                                        </div>
                                    </Col>
                                )
                            })
                        }




                    </Row>

                </Container>

            </div >
        )
    }
}



export default AxiosNew;