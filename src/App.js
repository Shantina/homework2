import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Clock from './Clock';
import AxiosNew from './AxiosNew';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Clock />
        <AxiosNew />
      </div>
    );
  }
}

export default App;
